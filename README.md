
# Overview

[Download a beta release from the Atlassian Marketplace](https://marketplace.atlassian.com/plugins/com.carolynvs.trade_depot)

This is a plugin for Atlassian Bamboo which enables you to export and import your plans. It exposes a REST API which you can use to selectively export, edit and then import plan definitions. In the future, it will support updating existing plans, for now only the creation of new plans is fully supported.

If you would like encourage Atlassian to provide this functionality, please vote for [Import Export individual projects / plans](https://jira.atlassian.com/browse/BAM-1223).

### Security Notes
Due to the nature of this plugin, rather than attempting to verify that a user has the appropriate permissions for the information being imported/exported, everything simply requires that the authenticated user is a Bamboo system administrator.

### Supported Features
There is a lot of information that could be included. However as I am not simply dumping serialized Bamboo objects, each piece of data is something that must be deliberately managed.  Here is a list of what is supported. Please [open create a new enhancement request](https://bitbucket.org/carolynvs/bamboo-trade-depot/issues/new) to vote for which features should be implemented next. 

*Items in bold are fully supported / feature complete*
 
* **Projects**
* Plans
	* **Stages**
	* Jobs
		* **Tasks**
		* **Artifacts**
	* Repositories
		* Git, Subversion, GitHub, Bitbucket and Perforce
		* Advanced features like webview, shallow cloning, etc are not yet supported
	* Triggers
		* **Repository Polling**
	* **Branches**
	* **Notifications**
	* **Variables**
* **Shared Repositories**
* **Shared Credentials**
* **Global Variables**
 
### REST API
All URLs are relative to the plugin root path, e.g. [http://localhost:6690/bamboo/rest/trade-depot/1.0](http://localhost:6690/bamboo/rest/trade-depot/1.0).

**/project**

* GET - Export all projects
* POST - Create a new project
* DELETE - Delete all projects

**/project/{project-key}**

* GET - Export the specified project
* DELETE - Delete the specified project

**/project/{project-key}/plan**

* GET - Export the specified project's plans

**/project/{project-key}/plan/{plan-key}**

* GET - Export the specified plan
* DELETE - Delete the specified plan

**/repository**

* GET - Export all shared repositories
* POST - Create a new shared repository

**/repository/{id}**

* GET - Export the specified repository

**/credentials**

* GET - Export all shared credentials
* POST - Create a new shared credentials

# Sample Requests
The JSON in these requests is assuredly out of date. Always export first to see what the correct format is, then use the exported data to craft an import request.

## Export All Projects
[src/test/scripts/export.sh](https://bitbucket.org/carolynvs/bamboo-trade-depot/raw/HEAD/src/test/scripts/export.sh)

*Example*

    curl http://localhost:6990/bamboo/rest/trade-depot/1.0/project \
    --header "Accept:application/json" \
    -u admin:admin

## Import a Project
[src/test/scripts/full-project-definition.json](src/test/scripts/full-project-definition.json?raw) - Sample project definiiton

[src/test/scripts/import-project.sh](src/test/scripts/run-integration-tests.sh?raw) - Script to create the above project

*Example*

    curl http://localhost:6990/bamboo/rest/trade-depot/1.0/project -d \
    '{
      "Key":"CS",
      "Name":"Cool Stuff",
      "Plans":[
        {
          "Key":"MP1",
          "Name":"My Plan 1"
        }
      ]
    }' \
    --header "Content-Type:application/json" \
    -u admin:admin