#!/bin/sh

curl --header "Accept:application/json" -u admin:admin http://localhost:6990/bamboo/rest/trade-depot/1.0/project
curl --header "Accept:application/json" -u admin:admin http://localhost:6990/bamboo/rest/trade-depot/1.0/repository
curl --header "Accept:application/json" -u admin:admin http://localhost:6990/bamboo/rest/trade-depot/1.0/credentials
curl --header "Accept:application/json" -u admin:admin http://localhost:6990/bamboo/rest/trade-depot/1.0/variable
