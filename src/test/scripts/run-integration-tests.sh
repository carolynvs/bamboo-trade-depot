#!/bin/sh
curDir=`dirname $0`

curl -d @"$curDir/shared-credentials.json" --header "Content-Type:application/json" -u admin:admin http://localhost:6990/bamboo/rest/trade-depot/1.0/credentials
curl -d @"$curDir/full-project-definition.json" --header "Content-Type:application/json" -u admin:admin http://localhost:6990/bamboo/rest/trade-depot/1.0/project
curl -d @"$curDir/minimal-project-definition.json" --header "Content-Type:application/json" -u admin:admin http://localhost:6990/bamboo/rest/trade-depot/1.0/project
