package it.com.carolynvs.bamboo.plugin.trade_depot;

import com.atlassian.bamboo.applinks.ImpersonationService;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.carolynvs.bamboo.plugin.trade_depot.*;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.Callable;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

@RunWith(AtlassianPluginsTestRunner.class)
public class PlanImportWiredTest
{
    private final ImpersonationService impersonationService;
    private final BuildExporter exporter;
    private BuildImporter importer;
    private int nextProject = 1;
    private int nextPlan = 1;
    private int nextCredentials = 1;

    private boolean isInitialized;

    public PlanImportWiredTest(ImpersonationService impersonationService, BuildExporter exporter, BuildImporter importer)
    {
        this.impersonationService = impersonationService;
        this.exporter = exporter;
        this.importer = importer;
    }

    //@Before
    public void FixtureSetup()
            throws Exception
    {
        if(isInitialized)
            return;

        impersonationService.runAsUser("admin", new Callable<Object>()
        {
            @Override
            public Object call() throws Exception
            {
                isInitialized = true;
                importer.deleteProjects();
                waitForDeletionThread();
                return null;
            }
        }).call();
    }

    @Test
    public void importMinimalPlan()
            throws Exception
    {
        ProjectBean project = createMinimalProject();

        ProjectBean result = importProject(project);

        assertNotNull(result);
        assertProjectExists(project.Key);
        assertPlanExists(project.Key, project.Plans.get(0).Key);
    }

    @Ignore("disable until full cleanup works properly")
    @Test
    public void deleteProject()
            throws Exception
    {
        impersonationService.runAsUser("admin", new Callable<Object>()
        {
            @Override
            public Object call() throws Exception
            {
                ProjectBean project = createMinimalProject();
                importer.importProject(project);

                importer.deleteProject(project.Key);

                assertProjectDoesNotExist(project.Key);
                assertPlanDoesNotExist(project.Key, project.Plans.get(0).Key);

                return null;
            }
        }).call();
    }

    @Test
    @Ignore("disable until full cleanup works properly")
    public void deletePlan()
            throws Exception
    {
        impersonationService.runAsUser("admin", new Callable<Object>()
        {
            @Override
            public Object call() throws Exception
            {
                ProjectBean project = createMinimalProject();
                String planKey = project.Plans.get(0).Key;
                importer.importProject(project);

                importer.deletePlan(project.Key, planKey);

                assertProjectExists(project.Key);
                assertPlanDoesNotExist(project.Key, planKey);

                return null;
            }
        }).call();

    }

    private ProjectBean importProject(final ProjectBean project)
            throws Exception
    {
        return impersonationService.runAsUser("admin", new Callable<ProjectBean>()
        {
            @Override
            public ProjectBean call() throws Exception
            {
                return importer.importProject(project);
            }
        }).call();
    }

    private ProjectBean createMinimalProject()
    {
        int projectId = nextProject++;
        int planId = nextPlan++;

        ProjectBean project = new ProjectBean();
        project.Key = String.format("PRJ%s", projectId);
        project.Name = String.format("My Project - %s", projectId);

        PlanBean plan = new PlanBean();
        project.Plans.add(plan);
        plan.Key = String.format("PLN%s", planId);
        plan.Name = String.format("My Plan - %s", planId);

        StageBean stage1 = new StageBean();
        stage1.Name = "My Stage 1";
        plan.Stages.add(stage1);

        JobBean job1 = new JobBean();
        stage1.Jobs.add(job1);
        job1.Key = "MJOB1";
        job1.Name = "My Job 1";

        TaskBean task1 = new TaskBean();
        job1.Tasks.add(task1);
        task1.Description = "Hello World";
        task1.Type = "com.atlassian.bamboo.plugins.scripttask:task.builder.script";
        task1.Configuration.put("argument", "");
        task1.Configuration.put("scriptBody", "echo \"Hello World!\"");
        task1.Configuration.put("runWithPowershell", "");
        task1.Configuration.put("workingSubDirectory", "");
        task1.Configuration.put("script", "");
        task1.Configuration.put("environmentVariables", "");
        task1.Configuration.put("scriptLocation", "INLINE");

        return project;
    }

    private void assertProjectExists(String projectKey)
    {
        ProjectBean project = exporter.exportProject(projectKey);
        assertNotNull(project);
    }

    private void assertProjectDoesNotExist(String projectKey)
    {
        waitForDeletionThread();
        ProjectBean project = exporter.exportProject(projectKey);
        assertNull(project);
    }

    private void assertPlanExists(String projectKey, String planKey)
    {
        PlanBean plan = exporter.exportPlan(projectKey, planKey);
        assertNotNull(plan);
    }

    private void assertPlanDoesNotExist(String projectKey, String planKey)
    {
        waitForDeletionThread();
        PlanBean plan = exporter.exportPlan(projectKey, planKey);
        assertNull(plan);
    }

    private void waitForDeletionThread()
    {
        try
        {
            Thread.sleep(1000);
        } catch (InterruptedException e) {}
    }
}