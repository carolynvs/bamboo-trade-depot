package it.com.carolynvs.bamboo.plugin.trade_depot;

import com.atlassian.bamboo.applinks.ImpersonationService;
import com.atlassian.plugins.osgi.test.AtlassianPluginsTestRunner;
import com.carolynvs.bamboo.plugin.trade_depot.*;

import com.carolynvs.bamboo.plugin.trade_depot.credentials.*;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.Callable;

import static org.junit.Assert.*;

@RunWith(AtlassianPluginsTestRunner.class)
public class RepositoryImportWiredTest
{
    private final ImpersonationService impersonationService;
    private final BuildExporter exporter;
    private BuildImporter importer;
    private int nextCredentials = 1;

    public RepositoryImportWiredTest(ImpersonationService impersonationService, BuildExporter exporter, BuildImporter importer)
    {
        this.impersonationService = impersonationService;
        this.exporter = exporter;
        this.importer = importer;
    }

    @Test
    public void importMinimalGitRepository()
            throws Exception
    {
        RepositoryBean repo = createMinimalGitRepository();
        RepositoryBean result = importRepository(repo);

        assertNotNull(result);
    }

    @Test
    public void importMinimalPerforceRepository()
            throws Exception
    {
        RepositoryBean repo = createMinimalPerforceRepository();
        RepositoryBean result = importRepository(repo);

        assertNotNull(result);
    }

    @Test
    public void importPerforceRepoWithEnvironmentVariables()
            throws Exception
    {
        RepositoryBean repo = createMinimalPerforceRepository();
        repo.Configuration.setProperty("repository.p4.environmentVariables", "v1=\"abc\"");

        RepositoryBean result = importRepository(repo);

        assertNotNull(result);
        assertTrue(result.Configuration.getString("repository.p4.environmentVariables").contains("v1"));
        assertTrue(result.Configuration.getString("repository.p4.environmentVariables").contains("abc"));
    }

    @Test
    public void importGitRepoWithSharedCredentials()
            throws Exception
    {
        SSHCredentialsBean credentials = createCredentials();
        importCredentials(credentials);

        RepositoryBean repo = createMinimalGitRepository();
        repo.Configuration.setProperty("repository.git.authenticationType", "SHARED_CREDENTIALS");
        repo.Configuration.setProperty("repository.git.sharedCrendentials", credentials.Name);

        RepositoryBean result = importRepository(repo);

        assertNotNull(result);
        assertTrue(result.Configuration.getString("repository.git.sharedCrendentials").contains(credentials.Name));
    }

    private SSHCredentialsBean createCredentials()
    {
        int credentialsId = nextCredentials++;

        SSHCredentialsBean credentials = new SSHCredentialsBean();
        credentials.Name = Integer.toString(credentialsId);
        credentials.PrivateKey = "top secret private key";
        credentials.Passphrase = "top secret passphrase";
        return credentials;
    }

    private CredentialsBean importCredentials(final CredentialsBean credentials)
            throws Exception
    {
        return impersonationService.runAsUser("admin", new Callable<CredentialsBean>()
        {
            @Override
            public CredentialsBean call() throws Exception
            {
                return importer.importSharedCredentials(credentials);
            }
        }).call();
    }

    private RepositoryBean importRepository(final RepositoryBean repo)
            throws Exception
    {
        return impersonationService.runAsUser("admin", new Callable<RepositoryBean>()
        {
            @Override
            public RepositoryBean call() throws Exception
            {
                return importer.importSharedRepository(repo);
            }
        }).call();
    }

    private RepositoryBean createMinimalGitRepository()
            throws Exception
    {
        RepositoryBean repo = new RepositoryBean();
        repo.Name = "atlassian-plugin-sdk";
        repo.Type = "com.atlassian.bamboo.plugins.atlassian-bamboo-plugin-git:git";
        repo.Configuration.setProperty("repository.git.branch", "master");
        repo.Configuration.setProperty("repository.git.authenticationType", "NONE");
        repo.Configuration.setProperty("repository.git.repositoryUrl", "https://github.com/carolynvs/atlassian-plugin-sdk.git");

        return repo;
    }

    private RepositoryBean createMinimalPerforceRepository()
    {
        RepositoryBean repo = new RepositoryBean();
        repo.Type = "com.atlassian.bamboo.plugin.system.repository:p4";
        repo.Name = "random perforce repo";
        repo.Configuration.setProperty("repository.p4.depot", "//foobar/...");
        repo.Configuration.setProperty("repository.p4.client", "foobar");
        repo.Configuration.setProperty("repository.p4.user", "secret_user");
        repo.Configuration.setProperty("repository.p4.password", "secret_password");
        return repo;
    }
}
