package com.carolynvs.bamboo.plugin.trade_depot;

import com.atlassian.bamboo.build.PlanCreationDeniedException;

public class BuildImporterImpl implements BuildImporter
{
    private final RepositoryService repositoryService;
    private final ProjectService projectService;
    private final PlanService planService;
    private final CredentialsService credentialsService;
    private final VariableService variableService;

    public BuildImporterImpl(RepositoryService repositoryService, ProjectService projectService, PlanService planService, CredentialsService credentialsService, VariableService variableService)
    {
        this.repositoryService = repositoryService;
        this.projectService = projectService;
        this.planService = planService;
        this.credentialsService = credentialsService;
        this.variableService = variableService;
    }

    @Override
    public void deleteProjects()
    {
        projectService.deleteAll();
    }

    @Override
    public void deleteProject(String key)
    {
        projectService.delete(key);
    }

    @Override
    public void deletePlan(String projectKey, String planKey)
    {
        planService.delete(projectKey, planKey);
    }

    @Override
    public void deleteGlobalVariable(String variableKey) { variableService.deleteGlobalVariable(variableKey); }

    @Override
    public ProjectBean importProject(ProjectBean projectBean)
            throws PlanCreationDeniedException
    {
        return projectService.save(projectBean);
    }

    @Override
    public RepositoryBean importSharedRepository(RepositoryBean repository)
    {
        return repositoryService.saveSharedRepository(repository);
    }

    @Override
    public CredentialsBean importSharedCredentials(CredentialsBean credentials)
    {
        return credentialsService.saveShared(credentials);
    }

    @Override
    public VariableBean importGlobalVariable(VariableBean variable) {
        return variableService.saveGlobalVariable(variable);
    }
}
