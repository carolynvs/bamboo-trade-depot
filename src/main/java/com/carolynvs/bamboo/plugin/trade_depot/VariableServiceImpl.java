package com.carolynvs.bamboo.plugin.trade_depot;

import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.plan.cache.ImmutableTopLevelPlan;
import com.atlassian.bamboo.variable.*;

import java.util.ArrayList;
import java.util.List;

public class VariableServiceImpl implements VariableService
{
    private final VariableDefinitionManager variableDefinitionManager;
    private final VariableDefinitionFactory variableDefinitionFactory;

    public VariableServiceImpl(VariableDefinitionManager variableDefinitionManager, VariableDefinitionFactory variableDefinitionFactory)
    {
        this.variableDefinitionManager = variableDefinitionManager;
        this.variableDefinitionFactory = variableDefinitionFactory;
    }

    @Override
    public List<VariableBean> getGlobal()
    {
        List<VariableBean> variables = new ArrayList<VariableBean>();
        for(VariableDefinition variable : variableDefinitionManager.getGlobalVariables())
        {
            variables.add(convertVariable(variable));
        }
        return variables;
    }

    @Override
    public List<VariableBean> getVariables(Chain plan)
    {
        List<VariableBean> variables = new ArrayList<VariableBean>();

        for(VariableDefinition variable : plan.getVariables())
        {
            variables.add(convertVariable(variable));
        }

        return variables;
    }

    @Override
    public VariableBean getGlobalByKey(String key)
    {
        return convertVariable(variableDefinitionManager.getGlobalVariableByKey(key));
    }

    @Override
    public VariableBean saveGlobalVariable(VariableBean variableBean)
    {
        // enforce uniqueness --
        // bamboo seems to not check this constraint, but you do actually need to do it
        VariableDefinition variable = variableDefinitionManager.getGlobalVariableByKey(variableBean.Key);

        if(variable == null)
        {
            VariableDefinitionFactory variableDefinitionFactory = new VariableDefinitionFactoryImpl();
            variable = variableDefinitionFactory.createGlobalVariable(variableBean.Key, variableBean.Value);
        }
        else
        {
            variable.setValue(variableBean.Value);
        }

        variableDefinitionManager.saveVariableDefinition(variable);
        return convertVariable(variable);
    }

    @Override
    public void savePlanVariables(Chain plan, List<VariableBean> variableBeans)
    {
        List<VariableDefinition> variables = new ArrayList<VariableDefinition>(variableBeans.size());
        for(VariableBean variableBean : variableBeans)
        {
            VariableDefinition variable = buildVariable(plan, variableBean);
            variables.add(variable);
        }

        variableDefinitionManager.saveVariableDefinitions(variables);
    }

    private VariableDefinition buildVariable(Chain plan, VariableBean variableBean)
    {
        return variableDefinitionFactory.createPlanVariable(plan, variableBean.Key, variableBean.Value);
    }

    @Override
    public void deleteGlobalVariable(String variableKey)
    {
        VariableDefinition searchVar = variableDefinitionManager.getGlobalVariableByKey(variableKey);
        if (searchVar == null)
            return;

        variableDefinitionManager.deleteVariableDefinition(searchVar);
    }

    private VariableBean convertVariable(VariableDefinition variable)
    {
        VariableBean variableBean = new VariableBean();

        variableBean.Key = variable.getKey();
        variableBean.Value = variable.getValue();

        return variableBean;
    }
}
