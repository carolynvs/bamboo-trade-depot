package com.carolynvs.bamboo.plugin.trade_depot;

import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.plan.cache.ImmutableTopLevelPlan;

import java.util.List;

public interface VariableService
{
    List<VariableBean> getGlobal();
    List<VariableBean> getVariables(Chain plan);
    VariableBean getGlobalByKey(String key);
    VariableBean saveGlobalVariable(VariableBean variable);
    void savePlanVariables(Chain plan, List<VariableBean> variables);
    void deleteGlobalVariable(String variableKey);
}

