package com.carolynvs.bamboo.plugin.trade_depot;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.*;
import org.codehaus.jackson.map.*;
import org.codehaus.jackson.node.*;
import org.json.*;

import java.io.*;

public class ConfigurationDataDeserializer extends JsonDeserializer<ConfigurationData>
{
    @Override
    public ConfigurationData deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
            throws IOException
    {
        // This only gets me a list of children, need to add back the root configuration node
        ObjectNode cfgRoot = deserializationContext.getNodeFactory().objectNode();
        JsonNode cfgChildren = jsonParser.getCodec().readTree(jsonParser);
        cfgRoot.put("configuration", cfgChildren);

        String configJson = cfgRoot.toString();
        String configXml = XML.toString(new JSONObject(configJson));
        return ConfigurationData.fromXml(configXml);
    }
}
