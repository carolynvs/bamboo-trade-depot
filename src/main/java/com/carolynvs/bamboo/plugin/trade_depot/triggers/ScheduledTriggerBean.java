package com.carolynvs.bamboo.plugin.trade_depot.triggers;

import com.atlassian.bamboo.trigger.TriggerTypeManager;
import com.carolynvs.bamboo.plugin.trade_depot.RepositoryBean;
import com.carolynvs.bamboo.plugin.trade_depot.TriggerBean;

import java.util.List;

public class ScheduledTriggerBean extends TriggerBean
{
    public static final String TYPE = TriggerTypeManager.BUILD_STRATEGY_SCHEDULED;

    public ScheduledTriggerBean()
    {
        this.Type = TYPE;
    }

    public String CronExpression;
}
