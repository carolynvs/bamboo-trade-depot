package com.carolynvs.bamboo.plugin.trade_depot;

import com.atlassian.bamboo.build.PlanCreationDeniedException;

public interface PlanService
{
    PlanBean get(String key);
    PlanBean get(String projectKey, String planKey);
    PlanBean save(ProjectBean project, PlanBean plan) throws PlanCreationDeniedException;
    void delete(String projectKey, String planKey);
}
