package com.carolynvs.bamboo.plugin.trade_depot.triggers;

import com.atlassian.bamboo.trigger.*;
import com.carolynvs.bamboo.plugin.trade_depot.*;

import java.util.*;

public class DailyBuildTriggerBean extends TriggerBean
{
    public static final String TYPE = TriggerTypeManager.BUILD_STRATEGY_DAILY;

    public DailyBuildTriggerBean()
    {
        this.Type = TYPE;
    }

    public String BuildTime;

}
