package com.carolynvs.bamboo.plugin.trade_depot;

import com.atlassian.bamboo.credentials.CredentialsData;
import com.atlassian.bamboo.credentials.CredentialsManager;
import com.atlassian.bamboo.security.EncryptionService;
import com.carolynvs.bamboo.plugin.trade_depot.credentials.CredentialsBuilder;
import com.carolynvs.bamboo.plugin.trade_depot.rest.BadRequestException;

import java.util.ArrayList;
import java.util.List;

public class CredentialsServiceImpl implements CredentialsService
{
    private final CredentialsManager credentialsManager;
    private final CredentialsBuilder credentialsBuilder;

    public CredentialsServiceImpl(CredentialsManager credentialsManager, EncryptionService encryptionService)
    {
        this.credentialsManager = credentialsManager;
        this.credentialsBuilder = new CredentialsBuilder(encryptionService);
    }

    @Override
    public CredentialsBean get(long id)
    {
        CredentialsData credentials = credentialsManager.getCredentials(id);
        return credentialsBuilder.build(credentials);
    }

    @Override
    public List<CredentialsBean> getShared()
    {
        List<CredentialsBean> results = new ArrayList<CredentialsBean>();
        for(CredentialsData credentials : credentialsManager.getAllCredentials())
        {
            results.add(credentialsBuilder.build(credentials));
        }
        return results;
    }

    @Override
    public CredentialsBean saveShared(CredentialsBean credentials)
    {
        CredentialsData credentialsData = credentialsBuilder.build(credentials);
        credentialsManager.createOrUpdateCredentials(credentialsData);
        return credentials;
    }

    public CredentialsData findSharedByName(String name)
    {
        for(CredentialsData credentials : credentialsManager.getAllCredentials())
        {
            if(credentials.getName().equals(name))
            {
                return credentials;
            }
        }

        throw new BadRequestException(String.format("Invalid shared credentials: %s", name));
    }
}
