package com.carolynvs.bamboo.plugin.trade_depot.rest;

import com.atlassian.bamboo.build.PlanCreationDeniedException;
import com.carolynvs.bamboo.plugin.trade_depot.*;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import com.sun.jersey.spi.container.ResourceFilters;

@Path("/project")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
@ResourceFilters({ RequireSystemAdministrator.class })
public class ProjectResource
{
    private final BuildExporter exporter;
    private final BuildImporter importer;

    public ProjectResource(BuildExporter exporter, BuildImporter importer)
    {
        this.exporter = exporter;
        this.importer = importer;
    }

    @GET
    public Response get()
    {
        List<ProjectBean> projects = exporter.exportProjects();
        return Response.ok(projects).build();
    }

    @DELETE
    public Response delete()
    {
        importer.deleteProjects();

        return Response.noContent().build();
    }

    @GET
    @Path("{project-key}")
    public Response get(@PathParam("project-key") String projectKey)
    {
        ProjectBean project = exporter.exportProject(projectKey);
        if(project == null)
            Response.status(Response.Status.NOT_FOUND).build();

        return Response.ok(project).build();
    }

    @PUT
    @Path("{project-key}")
    public Response update(@PathParam("project-key") String projectKey)
    {
        return Response.serverError().build();
    }

    @DELETE
    @Path("{project-key}")
    public Response delete(@PathParam("project-key") String projectKey)
    {
        importer.deleteProject(projectKey);

        return Response.noContent().build();
    }

    @POST
    public Response create(ProjectBean project)
    {
        try
        {
            project = importer.importProject(project);
            return Response.ok(project).build();
        }
        catch (PlanCreationDeniedException ex)
        {
            return Response.status(Response.Status.FORBIDDEN).build();
        }
    }

    @Path("{project-key}/plan")
    public PlanResource getPlan(@PathParam("project-key") String projectKey)
    {
        return new PlanResource(projectKey, exporter, importer);
    }
}


