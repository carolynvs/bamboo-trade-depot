package com.carolynvs.bamboo.plugin.trade_depot;

import com.atlassian.bamboo.utils.*;
import org.apache.commons.configuration.*;
import org.codehaus.jackson.map.annotate.*;
import org.w3c.dom.*;

import java.io.*;

@JsonDeserialize(using = ConfigurationDataDeserializer.class)
@JsonSerialize(using = ConfigurationDataSerializer.class)
public class ConfigurationData
{
    private XMLConfiguration xmlData = ConfigUtils.newConfiguration();

    public static ConfigurationData fromXml(String xml)
    {
        ConfigurationData configurationData = new ConfigurationData();
        configurationData.xmlData = ConfigUtils.getXmlConfigFromXmlString(xml);

        // Remove non-user configurable values
        removeRootAttributes(configurationData.xmlData);
        configurationData.xmlData.getRoot().removeChild("bambooDelimiterParsingDisabled");

        return configurationData;
    }

    private static void removeRootAttributes(XMLConfiguration xml)
    {
        Node root = xml.getDocument().getFirstChild();
        NamedNodeMap rootAttributes = root.getAttributes();

        for (int i = rootAttributes.getLength() - 1; i >= 0; i--)
        {
            String attrName = rootAttributes.item(i).getNodeName();
            rootAttributes.removeNamedItem(attrName);
        }
    }

    public String toXml()
    {
        StringWriter xmlWriter = new StringWriter();
        try
        {
            xmlData.save(xmlWriter);
        } catch (ConfigurationException e)
        {
            xmlWriter.write(e.getMessage());
        }

        return xmlWriter.toString();
    }

    public boolean containsKey(String key)
    {
        return xmlData.containsKey(key);
    }

    public void setProperty(String key, Object value)
    {
        xmlData.setProperty(key, value);
    }

    public String getString(String key)
    {
        return xmlData.getString(key);
    }

    public long getLong(String key)
    {
        return xmlData.getLong(key);
    }
}
