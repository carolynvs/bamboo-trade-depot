package com.carolynvs.bamboo.plugin.trade_depot.triggers;

import com.atlassian.bamboo.build.strategy.*;
import com.carolynvs.bamboo.plugin.trade_depot.TriggerBean;

public class ScheduledTriggerAdapter extends TriggerAdapter
{
    public static boolean supports(String type)
    {
        return ScheduledTriggerBean.TYPE.equals(type);
    }

    private static class ConfigKeys
    {
        public static final String CronExpression = "repository.change.schedule.cronExpression";
    }

    @Override
    protected void readConfiguration(BuildStrategy trigger, TriggerBean triggerBean)
    {
        super.readConfiguration(trigger, triggerBean);

        CronTriggerBuildStrategy scheduledTrigger = (CronTriggerBuildStrategy)trigger;
        ScheduledTriggerBean scheduledTriggerBean = (ScheduledTriggerBean)triggerBean;

        scheduledTriggerBean.CronExpression = scheduledTrigger.getCronExpression();
    }

    @Override
    protected void writeConfiguration(TriggerBean trigger, TriggerData triggerData)
    {
        super.writeConfiguration(trigger, triggerData);

        ScheduledTriggerBean scheduledTriggerBean = (ScheduledTriggerBean)trigger;

        triggerData.Configuration.setProperty(ConfigKeys.CronExpression, scheduledTriggerBean.CronExpression);
    }

    @Override
    protected TriggerBean createTriggerBean()
    {
        return new ScheduledTriggerBean();
    }
}

