package com.carolynvs.bamboo.plugin.trade_depot.rest;

import com.atlassian.bamboo.security.acegi.acls.BambooPermission;
import com.atlassian.bamboo.spring.ComponentAccessor;
import com.sun.jersey.spi.container.ContainerRequest;
import com.sun.jersey.spi.container.ContainerRequestFilter;
import com.sun.jersey.spi.container.ContainerResponseFilter;
import com.sun.jersey.spi.container.ResourceFilter;

public class RequireSystemAdministrator implements ResourceFilter, ContainerRequestFilter
{
    @Override
    public ContainerRequest filter(ContainerRequest containerRequest)
    {
        if (!ComponentAccessor.BAMBOO_PERMISSION_MANAGER.get().hasGlobalPermission(BambooPermission.ADMINISTRATION))
        {
            throw new ForbiddenException();
        }

        return containerRequest;
    }

    @Override
    public ContainerRequestFilter getRequestFilter() {
        return this;
    }

    @Override
    public ContainerResponseFilter getResponseFilter() {
        return null;
    }
}
