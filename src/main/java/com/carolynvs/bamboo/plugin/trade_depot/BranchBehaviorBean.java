package com.carolynvs.bamboo.plugin.trade_depot;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.chains.branches.BranchNotificationStrategy;
import com.atlassian.bamboo.plan.branch.BranchIntegrationConfiguration;
import com.atlassian.bamboo.plan.branch.BranchMonitoringConfiguration;
import org.apache.commons.configuration.HierarchicalConfiguration;

public class BranchBehaviorBean
{
    public BranchBehaviorBean()
    {}

    public BranchBehaviorBean(Chain plan)
    {
        BuildDefinition buildDefinition = plan.getBuildDefinition();
        BranchMonitoringConfiguration branchMonitoringConfiguration = buildDefinition.getBranchMonitoringConfiguration();
        BranchIntegrationConfiguration branchIntegrationConfiguration = branchMonitoringConfiguration.getDefaultBranchIntegrationConfiguration();

        ShouldAutomaticallyCreatePlanBranches = branchMonitoringConfiguration.isMonitoringEnabled();
        if (!ShouldAutomaticallyCreatePlanBranches)
            return;

        BranchPattern = branchMonitoringConfiguration.getMatchingPattern();
        MinimumInactiveDaysBeforeRemoval = branchMonitoringConfiguration.getTimeOfInactivityInDays();
        NotificationStrategy = branchMonitoringConfiguration.getDefaultBranchNotificationStrategy().getKey();
        ShouldLinkBranchToJira = branchMonitoringConfiguration.isRemoteJiraBranchLinkingEnabled();

        ShouldMergeBranch = branchIntegrationConfiguration.isEnabled();
        if (!ShouldMergeBranch)
            return;

        MergeStrategy = branchIntegrationConfiguration.getStrategy().name();
        MergeBranchName = branchIntegrationConfiguration.getIntegrationPlanBranchKey().getKey();
        ShouldPushMergeResult = branchIntegrationConfiguration.isPushEnabled();
    }

    public boolean ShouldAutomaticallyCreatePlanBranches;
    public String BranchPattern;
    public Integer MinimumInactiveDaysBeforeRemoval;
    public String NotificationStrategy;
    public Boolean ShouldLinkBranchToJira;
    public boolean ShouldMergeBranch;
    public String MergeStrategy;
    public String MergeBranchName;
    public Boolean ShouldPushMergeResult;

    public void applyToDefinition(BuildDefinition buildDefinition)
    {
        final String IntegrationEnabled  = "branches.defaultBranchIntegration.enabled";
        final String IntegrationStrategy = "branches.defaultBranchIntegration.strategy";
        final String Integration_BranchUpdater_Branch = "branches.defaultBranchIntegration.branchUpdater.mergeFromBranch";
        final String Integration_GateKeeper_Branch = "branches.defaultBranchIntegration.gateKeeper.checkoutBranch";
        final String Integration_BranchUpdater_PushEnabled = "branches.defaultBranchIntegration.branchUpdater.pushEnabled";
        final String Integration_GateKeeper_PushEnabled = "branches.defaultBranchIntegration.gateKeeper.pushEnabled";

        BranchMonitoringConfiguration monitoringCfg = buildDefinition.getBranchMonitoringConfiguration();
        monitoringCfg.setMonitoringEnabled(ShouldAutomaticallyCreatePlanBranches);
        if(ShouldAutomaticallyCreatePlanBranches)
        {
            monitoringCfg.setMatchingPattern(BranchPattern);
            monitoringCfg.setTimeOfInactivityInDays(MinimumInactiveDaysBeforeRemoval);
            monitoringCfg.setDefaultBranchNotificationStrategy(BranchNotificationStrategy.fromKey(NotificationStrategy));
            monitoringCfg.setRemoteJiraBranchLinkingEnabled(ShouldLinkBranchToJira);

            BranchIntegrationConfiguration integrationCfg = monitoringCfg.getDefaultBranchIntegrationConfiguration();
            HierarchicalConfiguration integrationCfgMap = integrationCfg.toConfiguration();
            integrationCfgMap.setProperty(IntegrationEnabled, ShouldMergeBranch);
            if(ShouldMergeBranch)
            {
                integrationCfgMap.setProperty(IntegrationStrategy, MergeStrategy);
                integrationCfgMap.setProperty(Integration_BranchUpdater_PushEnabled, ShouldPushMergeResult);
                integrationCfgMap.setProperty(Integration_GateKeeper_PushEnabled, ShouldPushMergeResult);
                integrationCfgMap.setProperty(Integration_BranchUpdater_Branch, MergeBranchName);
                integrationCfgMap.setProperty(Integration_GateKeeper_Branch, MergeBranchName);
            }

            integrationCfg.populateFromConfig(integrationCfgMap);
            monitoringCfg.setDefaultBranchIntegrationConfiguration(integrationCfg);
        }
    }
}