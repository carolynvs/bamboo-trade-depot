package com.carolynvs.bamboo.plugin.trade_depot.triggers;

import com.atlassian.bamboo.build.strategy.*;
import com.carolynvs.bamboo.plugin.trade_depot.*;

public class DailyBuildTriggerAdapter extends TriggerAdapter
{
    public static boolean supports(String type)
    {
        return DailyBuildTriggerBean.TYPE.equals(type);
    }

    private static class ConfigKeys
    {
        public static final String BuildTime = "repository.change.daily.buildTime";
    }

    @Override
    protected void readConfiguration(BuildStrategy trigger, TriggerBean triggerBean)
    {
        super.readConfiguration(trigger, triggerBean);

        SingleDailyBuildStrategy dailyTrigger = (SingleDailyBuildStrategy)trigger;
        DailyBuildTriggerBean dailyTriggerBean = (DailyBuildTriggerBean)triggerBean;

        dailyTriggerBean.BuildTime = dailyTrigger.getFormattedTime();
    }

    @Override
    protected void writeConfiguration(TriggerBean trigger, TriggerData triggerData)
    {
        super.writeConfiguration(trigger, triggerData);

        DailyBuildTriggerBean dailyTriggerBean = (DailyBuildTriggerBean)trigger;

        triggerData.Configuration.setProperty(ConfigKeys.BuildTime, dailyTriggerBean.BuildTime);
    }

    @Override
    protected TriggerBean createTriggerBean()
    {
        return new DailyBuildTriggerBean();
    }
}

