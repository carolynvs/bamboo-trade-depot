package com.carolynvs.bamboo.plugin.trade_depot;

import java.util.List;

public class BuildExporterImpl implements BuildExporter
{
    private final RepositoryService repositoryService;
    private final PlanService planService;
    private final ProjectService projectService;
    private final CredentialsService credentialsService;
    private final VariableService variableService;

    public BuildExporterImpl(RepositoryService repositoryService, ProjectService projectService, PlanService planService, CredentialsService credentialsService,
                             VariableService variableService)
    {
        this.repositoryService = repositoryService;
        this.planService = planService;
        this.projectService = projectService;
        this.credentialsService = credentialsService;
        this.variableService = variableService;
    }

    @Override
    public List<RepositoryBean> exportSharedRepositories()
    {
        return repositoryService.getShared();
    }

    @Override
    public RepositoryBean exportRepository(long id)
    {
        return repositoryService.get(id);
    }

    @Override
    public List<CredentialsBean> exportSharedCredentials()
    {
        return credentialsService.getShared();
    }

    @Override
    public List<ProjectBean> exportProjects()
    {
        return projectService.getAll();
    }

    @Override
    public ProjectBean exportProject(String key)
    {
        return projectService.get(key);
    }

    @Override
    public PlanBean exportPlan(String projectKey, String planKey)
    {
        return planService.get(projectKey, planKey);
    }

    @Override
    public List<VariableBean> exportGlobalVariables()
    {
        return variableService.getGlobal();
    }

    @Override
    public VariableBean exportGlobalVariable(String key) { return variableService.getGlobalByKey(key); }
}
