package com.carolynvs.bamboo.plugin.trade_depot.rest;

import com.carolynvs.bamboo.plugin.trade_depot.*;
import com.sun.jersey.spi.container.ResourceFilters;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/repository")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
@ResourceFilters({ RequireSystemAdministrator.class })
public class RepositoryResource
{
    private final BuildExporter exporter;
    private final BuildImporter importer;

    public RepositoryResource(BuildExporter exporter, BuildImporter importer)
    {
        this.exporter = exporter;
        this.importer = importer;
    }

    @GET
    public Response get()
    {
        List<RepositoryBean> repositories = exporter.exportSharedRepositories();
        return Response.ok(repositories).build();
    }

    @GET
    @Path("{id}")
    public Response get(@PathParam("id") long id)
    {
        RepositoryBean repository = exporter.exportRepository(id);
        return Response.ok(repository).build();
    }

    @POST
    public Response create(RepositoryBean repository)
    {
        repository = importer.importSharedRepository(repository);
        return Response.ok(repository).build();
    }
}
