package com.carolynvs.bamboo.plugin.trade_depot;

import com.atlassian.bamboo.repository.*;
import com.atlassian.bamboo.ww2.actions.build.admin.create.*;
import org.apache.commons.configuration.*;
import org.apache.commons.lang3.*;

import java.io.*;

public class RepositoryBuilder
{
    private static final String SharedCredentialsKey = "repository.git.sharedCrendentials"; // preserving typo, bamboo relies up on the misspelling

    private final CredentialsServiceImpl credentialsService;

    public RepositoryBuilder(CredentialsServiceImpl credentialsService)
    {
        this.credentialsService = credentialsService;
    }

    public RepositoryBean getLink(RepositoryData repository)
    {
        RepositoryBean bean = new RepositoryBean();

        bean.Name = repository.getName();
        bean.IsShared = repository.isGlobal();
        bean.Type = repository.getPluginKey();

        return bean;
    }

    public RepositoryBean getDefinition(RepositoryData repository)
    {
        RepositoryBean bean = getLink(repository);

        XMLConfiguration repoConfig = new XMLConfiguration(repository.getConfiguration());
        StringWriter repoConfigXml = new StringWriter();
        try
        {
            repoConfig.save(repoConfigXml);
        }
        catch (ConfigurationException e)
        {
            // todo: log configuration exception
            return null;
        }

        bean.Configuration = ConfigurationData.fromXml(repoConfigXml.toString());
        convertIdsToNames(bean);

        return bean;
    }

    public BuildConfiguration getConfiguration(RepositoryBean repository)
    {
        BuildConfiguration cfg = new BuildConfiguration(repository.Configuration.toXml());
        convertNamesToIds(cfg);

        return cfg;
    }

    private void convertIdsToNames(RepositoryBean bean)
    {
        if (!bean.Configuration.containsKey(SharedCredentialsKey))
            return;

        long credentialsId = bean.Configuration.getLong(SharedCredentialsKey);
        String credentialsName = credentialsService.get(credentialsId).Name;
        bean.Configuration.setProperty(SharedCredentialsKey, credentialsName);
    }

    private void convertNamesToIds(BuildConfiguration cfg)
    {
        if (!cfg.containsKey(SharedCredentialsKey))
            return;

        String credentialsName = cfg.getString(SharedCredentialsKey);
        if (StringUtils.isBlank(credentialsName))
            return;

        long credentialsId = credentialsService.findSharedByName(credentialsName).getId();
        cfg.setProperty(SharedCredentialsKey, credentialsId);
    }
}
