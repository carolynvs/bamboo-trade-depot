package com.carolynvs.bamboo.plugin.trade_depot.rest;

import com.carolynvs.bamboo.plugin.trade_depot.BuildExporter;
import com.carolynvs.bamboo.plugin.trade_depot.BuildImporter;
import com.carolynvs.bamboo.plugin.trade_depot.CredentialsBean;
import com.sun.jersey.spi.container.ResourceFilters;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/credentials")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
@ResourceFilters({ RequireSystemAdministrator.class })
public class CredentialResource
{
    private final BuildExporter exporter;
    private final BuildImporter importer;

    public CredentialResource(BuildExporter exporter, BuildImporter importer)
    {

        this.exporter = exporter;
        this.importer = importer;
    }

    @GET
    public Response get()
    {
        List<CredentialsBean> credentials = exporter.exportSharedCredentials();
        return Response.ok(credentials).build();
    }

    @POST
    public Response create(CredentialsBean credentials)
    {
        credentials = importer.importSharedCredentials(credentials);
        return Response.ok(credentials).build();
    }
}
