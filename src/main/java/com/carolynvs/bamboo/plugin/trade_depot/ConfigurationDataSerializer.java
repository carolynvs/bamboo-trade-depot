package com.carolynvs.bamboo.plugin.trade_depot;

import org.codehaus.jackson.*;
import org.codehaus.jackson.map.*;
import org.json.*;

import java.io.*;

public class ConfigurationDataSerializer extends JsonSerializer<ConfigurationData>
{
    @Override
    public void serialize(ConfigurationData configurationData, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
            throws IOException
    {
        String configXml = configurationData.toXml();


        // Do not serialize root node
        configXml = configXml
                .replace("<configuration>", "")
                .replace("</configuration>", "");

        String configJson = XML.toJSONObject(configXml).toString();

        jsonGenerator.writeRawValue(configJson.toString());
    }
}
