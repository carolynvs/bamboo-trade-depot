package com.carolynvs.bamboo.plugin.trade_depot.rest;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class ForbiddenException extends WebApplicationException
{
    private static final String Message = "This plugin requires that you authenticate as a System Administrator";

    public ForbiddenException()
    {
        super(Response.status(Response.Status.FORBIDDEN).type(MediaType.APPLICATION_JSON)
                .entity(Message).build());
    }
}
