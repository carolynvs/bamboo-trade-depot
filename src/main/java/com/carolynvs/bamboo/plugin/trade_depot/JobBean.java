package com.carolynvs.bamboo.plugin.trade_depot;

import java.util.ArrayList;
import java.util.List;

public class JobBean
{
    public String Key;
    public String Name;
    public String Description = "";
    public boolean IsEnabled = false;
    public List<TaskBean> Tasks = new ArrayList<TaskBean>();
    public List<ArtifactBean> Artifacts = new ArrayList<ArtifactBean>();
}
