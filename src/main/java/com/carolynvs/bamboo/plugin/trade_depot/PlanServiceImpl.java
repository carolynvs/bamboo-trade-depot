package com.carolynvs.bamboo.plugin.trade_depot;

import com.atlassian.bamboo.build.BuildDefinition;
import com.atlassian.bamboo.build.BuildDefinitionManager;
import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.build.PlanCreationDeniedException;
import com.atlassian.bamboo.build.creation.ChainCreationService;
import com.atlassian.bamboo.build.creation.JobCreationService;
import com.atlassian.bamboo.build.creation.PlanCreationService;
import com.atlassian.bamboo.build.strategy.BuildStrategyConfigurationService;
import com.atlassian.bamboo.caching.DashboardCachingManager;
import com.atlassian.bamboo.chains.Chain;
import com.atlassian.bamboo.chains.ChainStage;
import com.atlassian.bamboo.collections.ActionParametersMap;
import com.atlassian.bamboo.deletion.DeletionService;
import com.atlassian.bamboo.fieldvalue.TaskConfigurationUtils;
import com.atlassian.bamboo.notification.NotificationRule;
import com.atlassian.bamboo.notification.NotificationRuleImpl;
import com.atlassian.bamboo.notification.NotificationSet;
import com.atlassian.bamboo.notification.NotificationSetImpl;
import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.artifact.ArtifactDefinition;
import com.atlassian.bamboo.plan.artifact.ArtifactDefinitionManager;
import com.atlassian.bamboo.project.Project;
import com.atlassian.bamboo.repository.Repository;
import com.atlassian.bamboo.repository.nullrepository.NullRepository;
import com.atlassian.bamboo.task.TaskConfigurationService;
import com.atlassian.bamboo.task.TaskDefinition;
import com.atlassian.bamboo.task.TaskDefinitionImpl;
import com.atlassian.bamboo.webwork.util.ActionParametersMapImpl;
import com.atlassian.bamboo.ww2.actions.build.admin.create.BuildConfiguration;
import com.carolynvs.bamboo.plugin.trade_depot.triggers.TriggerBuilder;
import com.carolynvs.bamboo.plugin.trade_depot.triggers.TriggerData;
import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PlanServiceImpl implements PlanService
{
    private final PlanManager planManager;
    private final BuildDefinitionManager buildDefinitionManager;
    private final ChainCreationService chainCreationService;
    private final JobCreationService jobCreationService;
    private final TaskConfigurationService taskConfigurationService;
    private final BuildStrategyConfigurationService buildStrategyConfigurationService;
    private final ArtifactDefinitionManager artifactDefinitionManager;
    private final DeletionService deletionService;
    private final DashboardCachingManager dashboardCachingManager;
    private final RepositoryServiceImpl repositoryService;
    private final VariableService variableService;
    private final TriggerBuilder triggerBuilder;

    public PlanServiceImpl(PlanManager planManager, BuildDefinitionManager buildDefinitionManager,
                           ChainCreationService chainCreationService, JobCreationService jobCreationService,
                           TaskConfigurationService taskConfigurationService, BuildStrategyConfigurationService buildStrategyConfigurationService,
                           ArtifactDefinitionManager artifactDefinitionManager,
                           DeletionService deletionService, DashboardCachingManager dashboardCachingManager,
                           RepositoryServiceImpl repositoryService, VariableService variableService)
    {

        this.planManager = planManager;
        this.buildDefinitionManager = buildDefinitionManager;
        this.chainCreationService = chainCreationService;
        this.jobCreationService = jobCreationService;
        this.taskConfigurationService = taskConfigurationService;
        this.buildStrategyConfigurationService = buildStrategyConfigurationService;
        this.artifactDefinitionManager = artifactDefinitionManager;
        this.deletionService = deletionService;
        this.dashboardCachingManager = dashboardCachingManager;
        this.repositoryService = repositoryService;
        this.variableService = variableService;

        this.triggerBuilder = new TriggerBuilder(repositoryService);
    }

    @Override
    public PlanBean get(String key)
    {
        PlanKey fullKey = PlanKeys.getPlanKey(key);
        return get(fullKey);
    }

    @Override
    public PlanBean get(String projectKey, String planKey)
    {
        projectKey = projectKey.toUpperCase();
        planKey = planKey.toUpperCase();
        PlanKey key = PlanKeys.getPlanKey(projectKey, planKey);

        return get(key);
    }

    private PlanBean get(PlanKey key)
    {
        Chain plan = planManager.getPlanByKey(key, Chain.class);
        return convertPlan(plan);
    }

    @SuppressWarnings("deprecation")
    @Override
    public PlanBean save(ProjectBean projectBean, PlanBean planBean) throws PlanCreationDeniedException
    {
        BuildConfiguration chainCfg = new BuildConfiguration();

        chainCfg.addProperty(Repository.SELECTED_REPOSITORY, NullRepository.KEY);

        Map<String, Object> chainContext = Maps.newHashMap();
        chainContext.put(ChainCreationService.EXISTING_PROJECT_KEY, projectBean.Key);
        chainContext.put(ChainCreationService.CHAIN_KEY, planBean.Key);
        chainContext.put(ChainCreationService.CHAIN_NAME, planBean.Name);
        ActionParametersMap chainParams = new ActionParametersMapImpl(chainContext);

        PlanCreationService.EnablePlan isEnabled = planBean.IsEnabled ? PlanCreationService.EnablePlan.ENABLED : PlanCreationService.EnablePlan.DISABLED;
        String key = chainCreationService.createPlan(chainCfg, chainParams, isEnabled);
        PlanKey planKey = PlanKeys.getPlanKey(key);

        Chain plan = this.planManager.getPlanByKey(planKey, Chain.class);

        saveStages(planBean, plan);
        saveRepositories(planBean, plan);
        saveTriggers(planBean, plan);
        saveNotifications(planBean, plan);
        saveBranchBehavior(planBean, plan);
        variableService.savePlanVariables(plan, planBean.Variables);

        chainCreationService.triggerCreationCompleteEvents(planKey);
        dashboardCachingManager.updatePlanCache(planKey);

        return planBean;
    }

    private void saveBranchBehavior(PlanBean planBean, Chain plan)
    {
        BuildDefinition buildDefinition = plan.getBuildDefinition();

        planBean.BranchBehavior.applyToDefinition(buildDefinition);

        buildDefinitionManager.savePlanAndDefinition(plan, buildDefinition);
    }

    @Override
    public void delete(String projectKey, String planKey)
    {
        projectKey = projectKey.toUpperCase();
        planKey = planKey.toUpperCase();

        PlanKey key = PlanKeys.getPlanKey(projectKey, planKey);
        Plan plan = planManager.getPlanByKey(key);
        deletionService.deletePlan(plan);
        deletionService.executeDelayedDeletion();
    }

    public List<PlanBean> getPlans(Project project)
    {
        List<PlanBean> plans = new ArrayList<PlanBean>();

        for(Chain plan : planManager.getPlansByProject(project))
        {
            plans.add(convertPlan(plan));
        }

        return plans;
    }

    private PlanBean convertPlan(Chain plan)
    {
        if(plan == null)
            return null;

        PlanBean planBean = new PlanBean();

        planBean.Key = plan.getBuildKey();
        planBean.Name = plan.getBuildName();
        planBean.Description = plan.getDescription();
        planBean.IsEnabled =!plan.isSuspended();
        planBean.Stages = getStages(plan);
        planBean.Repositories = repositoryService.getRepositories(plan);
        planBean.Triggers = triggerBuilder.build(plan);
        planBean.Notifications = getNotifications(plan);
        planBean.Variables = variableService.getVariables(plan);
        planBean.BranchBehavior = new BranchBehaviorBean(plan);

        return planBean;
    }

    private List<StageBean> getStages(Chain plan)
    {
        List<StageBean> stages = new ArrayList<StageBean>();

        for(ChainStage stage : plan.getStages())
        {
            stages.add(convertStage(stage));
        }

        return stages;
    }

    private StageBean convertStage(ChainStage stage)
    {
        StageBean stageBean = new StageBean();

        stageBean.Name = stage.getName();
        stageBean.Description = stage.getDescription();
        stageBean.IsManual = stage.isManual();
        stageBean.Jobs = getJobs(stage);

        return stageBean;
    }

    private List<JobBean> getJobs(ChainStage stage)
    {
        List<JobBean> jobs = new ArrayList<JobBean>();

        for(Job job : stage.getJobs())
        {
            jobs.add(convertJob(job));
        }

        return jobs;
    }

    private JobBean convertJob(Job job)
    {
        JobBean jobBean = new JobBean();

        jobBean.Key = PlanKeys.getPartialJobKey(job.getPlanKey());
        jobBean.Name = job.getBuildName();
        jobBean.Description = job.getDescription();
        jobBean.IsEnabled = !job.isSuspendedFromBuilding();
        jobBean.Tasks = getTasks(job);
        jobBean.Artifacts = getArtifacts(job);

        return jobBean;
    }

    private List<TaskBean> getTasks(Job job)
    {
        List<TaskBean> tasks = new ArrayList<TaskBean>();
        BuildDefinition jobDefinition = buildDefinitionManager.getBuildDefinition(job.getPlanKey());

        for(TaskDefinition task : jobDefinition.getTaskDefinitions())
        {
            tasks.add(convertTask(task));
        }

        return tasks;
    }

    private TaskBean convertTask(TaskDefinition task)
    {
        TaskBean taskBean = new TaskBean();

        taskBean.Type = task.getPluginKey();
        taskBean.Description = task.getUserDescription();
        taskBean.IsEnabled = task.isEnabled();
        taskBean.IsFinal = task.isFinalising();
        taskBean.Configuration = task.getConfiguration();

        return taskBean;
    }

    private List<ArtifactBean> getArtifacts(Job job)
    {
        List<ArtifactBean> artifacts = new ArrayList<ArtifactBean>();

        for(ArtifactDefinition artifact : job.getArtifactDefinitions())
        {
            artifacts.add(new ArtifactBean(artifact));
        }

        return artifacts;
    }

    private List<NotificationBean> getNotifications(Chain plan)
    {
        List<NotificationBean> notifications = new ArrayList<NotificationBean>();

        for(NotificationRule notification : plan.getNotificationSet().getSortedNotificationRules())
        {
            notifications.add(convertNotification(notification));
        }

        return notifications;
    }

    private NotificationBean convertNotification(NotificationRule notification)
    {
        NotificationBean planBean = new NotificationBean();

        planBean.Type = notification.getConditionKey();
        planBean.Data = notification.getConditionData();
        planBean.RecipientType = notification.getRecipientType();
        planBean.RecipientData = notification.getRecipient();

        return planBean;
    }

    private void saveNotifications(PlanBean planBean, Chain plan)
    {
        NotificationSet notificationSet = new NotificationSetImpl();
        for (NotificationBean notificationBean : planBean.Notifications)
        {
            NotificationRuleImpl notification = new NotificationRuleImpl();
            notification.setConditionKey(notificationBean.Type);
            notification.setConditionData(notificationBean.Data);
            notification.setRecipientType(notificationBean.RecipientType);
            notification.setRecipient(notificationBean.RecipientData);
            notificationSet.addNotification(notification);
        }
        plan.setNotificationSet(notificationSet);
    }

    private void saveStages(PlanBean planBean, Chain plan)
            throws PlanCreationDeniedException
    {
        for(StageBean stageBean : planBean.Stages)
        {
            plan.addNewStage(stageBean.Name, stageBean.Description, stageBean.IsManual);
            planManager.savePlan(plan);

            saveJobs(plan, stageBean);
        }
    }

    private void saveJobs(Chain plan, StageBean stageBean)
            throws PlanCreationDeniedException
    {
        for(JobBean jobBean : stageBean.Jobs)
        {
            saveJob(plan, stageBean, jobBean);
        }
    }

    private void saveJob(Chain plan, StageBean stageBean, JobBean jobBean)
            throws PlanCreationDeniedException
    {
        BuildConfiguration jobCfg = new BuildConfiguration();
        Map<String, Object> jobContext = Maps.newHashMap();
        jobContext.put(JobCreationService.BUILD_KEY, plan.getKey());
        jobContext.put(JobCreationService.EXISTING_STAGE, stageBean.Name);
        jobContext.put(JobCreationService.STAGE_NAME, stageBean.Name);
        jobContext.put(JobCreationService.BUILD_NAME, jobBean.Name);
        jobContext.put(JobCreationService.SUB_BUILD_KEY, jobBean.Key);
        ActionParametersMap jobParams = new ActionParametersMapImpl(jobContext);

        PlanKey jobKey = jobCreationService.createJobAndBranches(jobCfg, jobParams, PlanCreationService.EnablePlan.ENABLED).get(0);
        jobCreationService.triggerCreationCompleteEvents(jobKey);

        List<TaskDefinition> tasks = buildTasks(jobBean.Tasks);
        taskConfigurationService.createTaskList(jobKey, tasks);

        Job job = planManager.getPlanByKey(jobKey, Job.class);
        saveArtifacts(jobBean, job);
    }

    private List<TaskDefinition> buildTasks(List<TaskBean> taskBeans)
    {
        List<TaskDefinition> tasks = new ArrayList<TaskDefinition>();

        for(TaskBean taskBean : taskBeans)
        {
            long taskId = TaskConfigurationUtils.getUniqueId(tasks);
            tasks.add(buildTask(taskId, taskBean));
        }

        return tasks;
    }

    private TaskDefinition buildTask(long taskId, TaskBean taskBean)
    {
        TaskDefinition task = new TaskDefinitionImpl(taskId, taskBean.Type, taskBean.Description, taskBean.IsEnabled, taskBean.Configuration);

        task.setFinalising(taskBean.IsFinal);

        return task;
    }

    private void saveRepositories(PlanBean planBean, Chain plan)
    {
        for(RepositoryBean repository : planBean.Repositories)
        {
            repositoryService.saveLocalRepository(repository, plan);
        }
    }

    private void saveTriggers(PlanBean planBean, Chain plan)
    {
        for(TriggerBean trigger : planBean.Triggers)
        {
            TriggerData data = triggerBuilder.getData(plan, trigger);

            buildStrategyConfigurationService.createBuildStrategy(data.Plan, data.Description, data.Repositories, data.Configuration);
        }
    }

    private void saveArtifacts(JobBean jobBean, Job job)
    {
        List<ArtifactDefinition> artifacts = new ArrayList<ArtifactDefinition>(jobBean.Artifacts.size());
        for(ArtifactBean artifact : jobBean.Artifacts)
        {
            artifacts.add(artifact.toDefinition(job));
        }

        artifactDefinitionManager.saveArtifactDefinitions(artifacts);
    }
}
