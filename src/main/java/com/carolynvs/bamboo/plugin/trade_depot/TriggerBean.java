package com.carolynvs.bamboo.plugin.trade_depot;

import com.carolynvs.bamboo.plugin.trade_depot.triggers.*;
import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonTypeInfo;

import java.util.*;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "Type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = PollingTriggerBean.class, name = PollingTriggerBean.TYPE),
        @JsonSubTypes.Type(value = ScheduledTriggerBean.class, name = ScheduledTriggerBean.TYPE),
        @JsonSubTypes.Type(value = DailyBuildTriggerBean.class, name = DailyBuildTriggerBean.TYPE)})
public class TriggerBean
{
    public String Type;
    public String Name;
    public String Description;
    public List<String> RequiredPassingPlanKeys;

    public boolean getShouldRequirePassingPlans()
    {
        return RequiredPassingPlanKeys != null && RequiredPassingPlanKeys.size() > 0;
    }
}
