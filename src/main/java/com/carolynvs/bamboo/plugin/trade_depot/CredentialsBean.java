package com.carolynvs.bamboo.plugin.trade_depot;

import com.carolynvs.bamboo.plugin.trade_depot.credentials.SSHCredentialsBean;
import org.codehaus.jackson.annotate.JsonSubTypes;
import org.codehaus.jackson.annotate.JsonTypeInfo;

@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "Type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = SSHCredentialsBean.class, name = SSHCredentialsBean.TYPE) })
public abstract class CredentialsBean
{
    public String Name;
    public String Type;
}

