package com.carolynvs.bamboo.plugin.trade_depot.rest;

import com.carolynvs.bamboo.plugin.trade_depot.BuildExporter;
import com.carolynvs.bamboo.plugin.trade_depot.BuildImporter;
import com.carolynvs.bamboo.plugin.trade_depot.VariableBean;
import com.sun.jersey.spi.container.ResourceFilters;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/variable")
@Produces({MediaType.APPLICATION_JSON})
@Consumes({MediaType.APPLICATION_JSON})
@ResourceFilters({ RequireSystemAdministrator.class })
public class VariableResource {

    private final BuildExporter exporter;
    private final BuildImporter importer;

    public VariableResource(BuildExporter exporter, BuildImporter importer)
    {
        this.exporter = exporter;
        this.importer = importer;
    }

    @GET
    public Response get()
    {
        List<VariableBean> variables = exporter.exportGlobalVariables();
        if(variables != null) {
            return Response.ok(variables).build();
        } else {
            return Response.noContent().build();
        }
    }

    @GET
    @Path("{key}")
    public Response get(@PathParam("key") String key) {
        VariableBean variable = exporter.exportGlobalVariable(key);
        if(variable != null) {
            return Response.ok(variable).build();
        } else {
            return Response.noContent().build();
        }
    }

    // create by full VariableBean in JSON in body
    @POST
    public Response create(VariableBean variableBean) {
        VariableBean newvar = importer.importGlobalVariable(variableBean);
        return Response.ok(newvar).build();
    }

    // updating a whole VariableBean via JSON
    @PUT
    public Response update(VariableBean variableBean) {
        VariableBean newvar = importer.importGlobalVariable(variableBean);
        return Response.ok(newvar).build();
    }

    // update with key in path, value as plain string in body
    @PUT
    @Path("{key}")
    public Response update(@PathParam("key") String key, String value) {
        VariableBean variableBean = new VariableBean();
        variableBean.Key = key;
        variableBean.Value = value;
        VariableBean newvar = importer.importGlobalVariable(variableBean);
        return Response.ok(newvar).build();
    }

    // delete by key in path
    @DELETE
    @Path("{key}")
    public Response delete(@PathParam("key") String key) {
        importer.deleteGlobalVariable(key);
        return Response.noContent().build();
    }
}
