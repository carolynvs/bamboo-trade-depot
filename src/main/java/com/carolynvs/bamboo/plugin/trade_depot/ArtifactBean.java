package com.carolynvs.bamboo.plugin.trade_depot;

import com.atlassian.bamboo.build.Job;
import com.atlassian.bamboo.plan.artifact.ArtifactDefinition;
import com.atlassian.bamboo.plan.artifact.ArtifactDefinitionImpl;
import com.atlassian.bamboo.plan.artifact.ImmutableArtifactDefinition;

public class ArtifactBean
{
    public ArtifactBean(){}

    public ArtifactBean(ImmutableArtifactDefinition artifact)
    {
        Name = artifact.getName();
        IsShared = artifact.isSharedArtifact();
        Location = artifact.getLocation();
        CopyPattern = artifact.getCopyPattern();
    }

    public String Name;
    public Boolean IsShared;
    public String Location;
    public String CopyPattern;

    public ArtifactDefinition toDefinition(Job job)
    {
        ArtifactDefinitionImpl definition = new ArtifactDefinitionImpl();

        definition.setName(Name);
        definition.setLocation(Location);
        definition.setCopyPattern(CopyPattern);
        definition.setSharedArtifact(IsShared);
        definition.setProducerJob(job);

        return definition;
    }
}
