package com.carolynvs.bamboo.plugin.trade_depot.triggers;

import com.atlassian.bamboo.build.strategy.*;
import com.atlassian.bamboo.chains.Chain;
import com.carolynvs.bamboo.plugin.trade_depot.TriggerBean;
import org.apache.commons.lang.*;

import java.util.*;

public abstract class TriggerAdapter
{
    private static class ConfigKeys
    {
        public static final String BuildStrategy = "selectedBuildStrategy";
        public static final String ShouldRequirePassingPlans = "custom.triggerrCondition.plansGreen.enabled";
        public static final String RequiredPassingPlanKeys = "custom.triggerrCondition.plansGreen.plans";
    }

    protected Chain plan;

    public final TriggerBean getDefinition(BuildStrategy trigger)
    {
        TriggerBean triggerBean = createTriggerBean();

        triggerBean.Name = trigger.getName();
        triggerBean.Description = trigger.getUserDescription();

        readConfiguration(trigger, triggerBean);
        return triggerBean;
    }

    protected abstract TriggerBean createTriggerBean();

    protected void readConfiguration(BuildStrategy trigger, TriggerBean triggerBean)
    {
        triggerBean.RequiredPassingPlanKeys = getRequirePassingPlansBeforeBuild(trigger);
    }

    protected void writeConfiguration(TriggerBean triggerBean, TriggerData triggerData)
    {
        triggerData.Plan = plan.getPlanKey();
        triggerData.Description = triggerBean.Description;

        triggerData.Configuration.setProperty(ConfigKeys.BuildStrategy, triggerBean.Type);

        String requiredPassingPlans = buildRequiredPassingPlans(triggerBean);
        triggerData.Configuration.setProperty(ConfigKeys.ShouldRequirePassingPlans, triggerBean.getShouldRequirePassingPlans());
        triggerData.Configuration.setProperty(ConfigKeys.RequiredPassingPlanKeys, requiredPassingPlans);
    }

    protected void initialize(Chain plan)
    {
        this.plan = plan;
    }

    public final TriggerData getData(Chain plan, TriggerBean trigger)
    {
        initialize(plan);

        TriggerData data = new TriggerData();

        writeConfiguration(trigger, data);

        return data;
    }

    private String buildRequiredPassingPlans(TriggerBean triggerBean)
    {
        return StringUtils.join(triggerBean.RequiredPassingPlanKeys, ';');
    }

    private List<String> getRequirePassingPlansBeforeBuild(BuildStrategy trigger)
    {
        Map<String,String> cfg = trigger.getTriggerConditionsConfiguration();
        String planKeys = cfg.get(ConfigKeys.RequiredPassingPlanKeys);
        if(planKeys == null)
            return null;

        return Arrays.asList(StringUtils.split(planKeys, ';'));
    }
}
