package com.carolynvs.bamboo.plugin.trade_depot.triggers;

import com.atlassian.bamboo.build.strategy.BuildStrategy;
import com.atlassian.bamboo.build.strategy.PollingBuildStrategy;
import com.carolynvs.bamboo.plugin.trade_depot.RepositoryBean;
import com.carolynvs.bamboo.plugin.trade_depot.RepositoryService;
import com.carolynvs.bamboo.plugin.trade_depot.TriggerBean;

import java.util.*;

public class PollingTriggerAdapter extends TriggerAdapter
{
    public static boolean supports(String type)
    {
        return PollingTriggerBean.TYPE.equals(type);
    }

    private static class ConfigKeys
    {
        public static final String CronExpression = "repository.change.poll.cronExpression";
        public static final String PollingFrequency = "repository.change.poll.pollingPeriod";
        public static final String PollingStrategy = "repository.change.poll.type";
    }

    private static final String DefaultCronExpression = "0 0 0 ? * *";

    private final RepositoryService repositoryService;

    public PollingTriggerAdapter(RepositoryService repositoryService)
    {
        this.repositoryService = repositoryService;
    }

    @Override
    protected void readConfiguration(BuildStrategy trigger, TriggerBean triggerBean)
    {
        super.readConfiguration(trigger, triggerBean);

        PollingBuildStrategy pollingTrigger = (PollingBuildStrategy)trigger;
        PollingTriggerBean pollingTriggerBean = (PollingTriggerBean)triggerBean;

        pollingTriggerBean.PollingStrategy = pollingTrigger.getPollingStrategy();
        pollingTriggerBean.PollingFrequency = pollingTrigger.getPollingPeriod();
        pollingTriggerBean.Repositories = getRepositoryLinks(pollingTrigger);
    }

    @Override
    protected void writeConfiguration(TriggerBean trigger, TriggerData triggerData)
    {
        super.writeConfiguration(trigger, triggerData);

        PollingTriggerBean pollingTriggerBean = (PollingTriggerBean)trigger;

        triggerData.Repositories = getRepositoryIds(pollingTriggerBean);
        triggerData.Configuration.setProperty(ConfigKeys.PollingStrategy, pollingTriggerBean.PollingStrategy);
        triggerData.Configuration.setProperty(ConfigKeys.PollingFrequency, pollingTriggerBean.PollingFrequency);
        triggerData.Configuration.setProperty(ConfigKeys.CronExpression, DefaultCronExpression);
    }

    private HashSet<Long> getRepositoryIds(PollingTriggerBean trigger)
    {
        List<Long> ids = repositoryService.lookupIds(plan, trigger.Repositories);
        return new HashSet<Long>(ids);
    }

    @Override
    protected TriggerBean createTriggerBean()
    {
        return new PollingTriggerBean();
    }

    private List<RepositoryBean> getRepositoryLinks(PollingBuildStrategy trigger)
    {
        List<RepositoryBean> repositories = new ArrayList<RepositoryBean>();

         for(long repositoryId : trigger.getTriggeringRepositories())
         {
             repositories.add(repositoryService.getLink(repositoryId));
         }

        return repositories;
    }
}

