package com.carolynvs.bamboo.plugin.trade_depot;

import java.util.ArrayList;
import java.util.List;

public class StageBean
{
    public String Name;
    public String Description = "";
    public boolean IsManual = false;
    public List<JobBean> Jobs = new ArrayList<JobBean>();
}
