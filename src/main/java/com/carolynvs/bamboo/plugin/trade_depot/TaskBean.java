package com.carolynvs.bamboo.plugin.trade_depot;

import java.util.HashMap;
import java.util.Map;

public class TaskBean
{
    public String Type;
    public String Description = "";
    public boolean IsEnabled = true;
    public boolean IsFinal = false;
    public Map<String, String> Configuration = new HashMap<String, String>();
}
