package com.carolynvs.bamboo.plugin.trade_depot;

import com.atlassian.bamboo.build.PlanCreationDeniedException;

import java.util.List;

public interface ProjectService
{
    ProjectBean get(String key);
    List<ProjectBean> getAll();
    ProjectBean save(ProjectBean project) throws PlanCreationDeniedException;
    void delete(String key);
    void deleteAll();
}
